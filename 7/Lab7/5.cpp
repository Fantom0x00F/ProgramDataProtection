#include <iostream>
#include <cstring>

using namespace std;

class String
{
private:
    char * str;             // pointer to string
    int len;                // length of string
    static int num_strings; // number of objects
    static const int CINLIM = 80;  // cin input limit
public:
// constructors and other methods
    String(const char * s); // constructor
    String();               // default constructor
    String(const String &); // copy constructor
    ~String();              // destructor
    int length () const { return len; }
// overloaded operator methods
    String & operator=(const String &);
    String & operator=(const char *);
    char & operator[](int i);
    const char & operator[](int i) const;
// overloaded operator friends
    friend bool operator<(const String &st, const String &st2);
    friend bool operator>(const String &st1, const String &st2);
    friend bool operator==(const String &st, const String &st2);
    friend ostream & operator<<(ostream & os, const String & st);
    friend istream & operator>>(istream & is, String & st);
// static function
    static int HowMany();
};

String::String(){
    str = new char[10];
    strcpy(str,"");
    len = 0;
    num_strings++;
}

String::String(const char * s){
    str = new char[strlen(s)];
    strcpy(str,s);
    len = strlen(s);
    num_strings++;
}

String::~String(){
    delete[] str;
    num_strings--;
}

String& String::operator=(const String& s)
{
    delete[] str;
    str = new char[s.length()];
    len = s.length();
    return *this;
}

String& String::operator=(const char *s)
{
    delete[] str;
    str = new char[strlen(s)];
    strcpy(str,s);
    len = strlen(s);
    return *this;
}

char& String::operator[](int i)
{
    return str[i];
}

int main()
{

    return 0;
}
