#include <iostream>
#include <cstring>

using namespace std;

class String
{
public:
    char * str;             // pointer to string
    int len;                // length of string
    static int num_strings; // number of objects
    static const int CINLIM = 80;  // cin input limit

// constructors and other methods
    String(const char * s); // constructor
    String();               // default constructor
    //String(const String &); // copy constructor
    ~String();              // destructor
    int length () const { return len; }
// overloaded operator methods
    String & operator=(const String &);
    String & operator=(const char *);
    char & operator[](int i);
    const char & operator[](int i) const;
    String& operator+(String &);
// overloaded operator friends
    friend bool operator<(const String &st, const String &st2);
    friend bool operator>(const String &st1, const String &st2);
    friend bool operator==(const String &st, const String &st2);
    friend ostream & operator<<(ostream & os, const String & st);
    friend istream & operator>>(istream & is, String & st);
// static function
    static int HowMany();
};

int String::num_strings = 0;

String::String(){
    str = new char[10];
    strcpy(str,"");
    len = 0;
    num_strings++;
    cout<<"Constructed "<<str<<". "<<num_strings<<endl;
}

String::String(const char * s){
    str = new char[strlen(s)];
    strcpy(str,s);
    len = strlen(s);
    num_strings++;
    cout<<"Constructed "<<str<<". "<<num_strings<<endl;
}

String::~String(){
    num_strings--;
    cout<<"Deleted "<<str<<". There are "<<num_strings<<endl;
    delete[] str;
}


String& String::operator=(const String& s)
{
    delete[] str;
    str = new char[s.length()];
    strcpy(str,s.str);
    len = s.length();
    return *this;
}



String& String::operator=(const char *s)
{
    delete[] str;
    str = new char[strlen(s)];
    strcpy(str,s);
    len = strlen(s);
    return *this;
}


char& String::operator[](int i)
{
    return str[i];
}


String& String::operator+(String& c){
    char* newStr = new char[strlen(str)+c.length()];
    strcpy(newStr,str);
    strcat(newStr,c.str);
    String* s = new String(newStr);
    delete[] newStr;
    return *s;
}

int main()
{
    String t("test");
    String g("test2");
    String n = g+t;
    cout<<"--------------------"<<endl;
    return 0;
}
