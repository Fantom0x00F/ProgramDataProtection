#include <iostream>
#include <cstring>

using namespace std;

class Cow {
	char name[20];
	char * hobby;
	double weight;
public:
	Cow();
	Cow(const char * nm, const char * ho, double wt);
	Cow(const Cow & c);
	~Cow() ;
	Cow & operator=(const Cow & c) ;
	void ShowCow() const; // ����������� ���� ������ cow
};

Cow::Cow(){
    strcpy(name,"");
    hobby = new char[10];
    strcpy(hobby,"");
    weight = 0;
}

Cow::Cow(const char * nm, const char * ho, double wt){
    strcpy(name,nm);
    hobby = new char[strlen(ho)];
    strcpy(hobby,ho);
    weight = wt;
}

Cow::Cow(const Cow& c){
    strcpy(name,c.name);
    hobby = new char[strlen(c.hobby)];
    strcpy(hobby,c.hobby);
    weight = c.weight;
}

Cow::~Cow(){
    delete [] name;
    delete [] hobby;
}

Cow& Cow::operator=(const Cow & c)
{
    strcpy(name,c.name);
    delete[] hobby;
    hobby = new char[strlen(c.hobby)];
    strcpy(hobby,c.hobby);
    weight = c.weight;
}

void Cow::ShowCow() const{
    cout<<name<<" "<<hobby<<" "<<weight;
}

int main()
{
    Cow c("example","eeee",0);
    Cow w = c;
    w.ShowCow();
    return 0;
}
