Лабораторная работа 07
Классы и динамическое выделение памяти

01. Пусть класс String содержит следующие закрытые члены: 
class String{ 
private: 
	char * str; // указывает на строку, распределенную операцией new 
	int len; // хранит длину строки 
}; 
а. Что неправильно в следующем конструкторе по умолчанию? 
String::String() {} 
б. Что неправильно в следующем конструкторе? 
String::String(const char * s){ 
	str = s; 
	len = strlen (s); 
} 
в. Что неправильно в следующем конструкторе? 
String::String(const char * s){ 
	strcpy(str, s); 
	len = strlen (s); 
} 

02. Найдите и исправьте ошибки в следующем объявлении класса: 
class nifty { 
// Данные 
	char personality[]; 
	int talents; 
// Методы 
	nifty(); 
	nifty(char * s); 
	ostream & operator<<(ostream & os, nifty & n); 
} 
nifty:nifty(){ 
	personality = NULL; 
	talents = 0; 
} 
nifty:nifty(char * s){ 
	personality = new char [strlen (s)]; 
	personality = s; 
	talents = 0; 
} 
ostream & nifty:operator<<(ostream & os, nifty & n){ 
	os << n; 
} 

03. Имеется следующее объявление класса: 
class Golfer { 
private: 
	char * fullname; // указывает на строку, содержащую имя игрока в гольф 
	int games; // хранит количество сыгранных игр 
	int * scores; // указывает на первый элемент массива счетов игр 
public: 
	Golfer(); 
	Golfer(const char * name, int g = 0); 
// Создает пустой динамический массив из g элементов, если g > 0 
	Golfer(const Golfer & g); 
	-Golfer(); 
}; 
а. Какие методы класса будут вызываться следующими операторами? 
	Golfer nancy; 						// #1 
	Golfer lulu("Little Lulu"); 		// #2 
	Golfer roy("Roy Hobbs", 12); 		// #3 
	Golfer * par = new Golfer; 			// #4 
	Golfer next = lulu; 				// #5 
	Golfer hazzard = "Weed Thwacker"; 	// #6 
	*par = nancy; 						// #7 
	nancy = "Nancy Putter"; 			// #8 
б. Ясно, что классу требуется больше методов для того, чтобы он был  
действительно полезным. Какой дополнительный метод нужен для защиты данных от 
разрушения? 

04. Имеется следующее объявление класса: 
class Cow { 
	char name[20]; 
	char * hobby; 
	double weight; 
public: 
	Cow(); 
	Cow(const char * nm, const char * ho, double wt); 
	Cow(const Cow & c); 
	~Cow() ; 
	Cow & operator=(const Cow & c) ; 
	void ShowCow() const; // отображение всех данных cow 
}; 
Напишите реализацию для этого класса и короткую программу, использующую 
все функции-члены. 

05. Имеется объявление класса
class String
{
private:
    char * str;             // pointer to string
    int len;                // length of string
    static int num_strings; // number of objects
    static const int CINLIM = 80;  // cin input limit
public:
// constructors and other methods
    String(const char * s); // constructor
    String();               // default constructor
    String(const String &); // copy constructor
    ~String();              // destructor
    int length () const { return len; }
// overloaded operator methods    
    String & operator=(const String &);
    String & operator=(const char *);
    char & operator[](int i);
    const char & operator[](int i) const;
// overloaded operator friends
    friend bool operator<(const String &st, const String &st2);
    friend bool operator>(const String &st1, const String &st2);
    friend bool operator==(const String &st, const String &st2);
    friend ostream & operator<<(ostream & os, const String & st);
    friend istream & operator>>(istream & is, String & st);
// static function
    static int HowMany();
};
Напишите реализацию этого класса.

06. Усовершенствуйте объявление класса String, выполнив перечисленные ниже действия. 
а. Перегрузите операцию + для объединения двух строк в одну. 
б. Напишите функцию-член stringlow(), которая преобразует все буквенные 
символы в строке в нижний регистр. (Не забудьте о семействе cctype  
символьных функций.) 
в. Напишите функцию-член stringup(), которая преобразует все буквенные 
символы в строке в верхний регистр. 
г. Напишите функцию-член, которая принимает аргумент типа char и  
возвращает количество раз, которое символ появляется в строке. 

Проверьте работу полученного класса в следующей программе: 

#include <iostream> 
using namespace std; 
#include "string.h" 
int main(){ 
String sl(" and I am a C++ student."); 
String s2 = "Please enter your name: "; // ввод имени 
String s3; 
cout << s2; // перегруженная операция << 
cin >> s3; // перегруженная операция >> 
s2 = "My name is " + s3; // перегруженные операции =, + 
cout<< s2 << ".\n"; 
s2= s2 + s1; 
s2.stringup(); // преобразование строки в верхний регистр 
cout << "The string\n" << s2 << "\ncontains " << s2.has('A') 
<< " 'A' characters in it.\n"; 
si = "red"; // String(const char *) , 
// тогда String & operator= (const Strings) 
String rgb[3] = { String (si), String("green"), String("blue")} ; 
cout << "Enter the name of a primary color for mixing light: " ; // ввод цвета 
String ans; 
bool success = false; 
while (cin >> ans){ 
ans.stringlow(); // преобразование строки в нижний регистр 
for(int i = 0; i < 3; i++){ 
if (ans == rgb[i]) // перегруженная операция == 
{ 
cout << "That's right!\n"; 
success = true; 
break; 
} 
} 
if (success) 
break; 
else 
cout << "Try again! \n"; 
} 
cout << "Bye\n"; 
return 0; 
} 
Вывод программы должен выглядеть приблизительно так: 
Please enter your name: Fretta Farbo 
My name is Fretta Farbo. 
The string 
MY NAME IS FRETTA FARBO AND I AM A C++ STUDENT. 
contains 6 'A' characters in it. 
Enter the name of a primary color for mixing light: yellow 
Try again! 
BLUE 
That's right! 
Bye 

07. Имеется следующий вариант класса Stack
// stack.h — объявление класса для АТД стека 
typedef unsigned long Item; 
class Stack{ 
private: 
enum {MAX = 10}; // константа, специфичная для класса 
Item * pitems; // хранит элементы стека 
int size; // количество элементов в стеке 
int top; // индекс для верхнего элемента стека 
public: 
Stack (int n = 10); // создает стек с n элементами 
Stack(const Stack & st); 
-Stack(); 
bool isempty() const; 
bool isfull() const; 
// push() возвращает значение false, если стек уже полный, 
// и true в противном случае 
bool push (const Item & item); // добавление элемента в стек 
// pop() возвращает значение false, если стек уже пустой, 
// и true в противном случае 
bool pop(Item & item); // извлечение элемента из стека 
Stack & operator=(const Stack & st); 
); 
Как понятно из закрытых членов, данный класс использует динамически  
выделенный массив для хранения элементов стека. Напишите методы для  
этого класса и напишите программу, которая демонстрирует работу 
всех методов, включая конструктор копирования и операцию присваивания. 
