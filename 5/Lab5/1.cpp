#include <iostream>

using namespace std;

void printstr(char *str,int mode =0)
{
    static int countOfCall = 0;
    if (mode){
        for (int i = 0 ; i < countOfCall ; i++)
            cout<<str<<endl;
    }
    else{
        cout<<str;
    }
    countOfCall++;
}

int main()
{
    char *str = "Hello world!";
    int n;
    while(true){
        cin>>n;
        printstr(str,n);
    }
    return 0;
}
