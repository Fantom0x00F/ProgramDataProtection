#include <iostream>
#include <string>
#include <math.h>

using namespace std;

struct Test {
    string name;
    int x;
    int y;
    float p;
    Test(string name="unknown",int x=1,int y=1)
    {
        this->name = name;
        this->x = x;
        this->y = y;
    }
};

const Test clone(Test & t)
{
	return t;
}

int main()
{
    Test t1;
    t1.x=2;
    Test t2=clone(t1);
    t1.x=5;
    cout<<t2.x;
    return 0;
}
