#include <iostream>
#include <string>

using namespace std;

struct cadyBar {
    char* name;
    float weight;
    int colories;
};

cadyBar &cake (cadyBar &cB, char *name="Millennium Munch", double weight = 2.85, int col = 350) {
    cB.name = name;
    cB.weight = weight;
    cB.colories = col;
    return cB;
}

void showCady (const cadyBar &cB) {
    cout << cB.name << endl;
    cout << cB.weight << endl;
    cout << cB.colories << endl;
}

int main ()
{
    cadyBar cB;
    cake(cB, "AAAAA", 2.5);
    showCady(cB);
    return 0;
}
