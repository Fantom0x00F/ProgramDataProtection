#include <iostream>
#include <string>
#include <math.h>

using namespace std;

struct Test {
    string name;
    int x;
    int y;
    float p;
    Test(string name="unknown",int x=1,int y=1)
    {
        this->name = name;
        this->x = x;
        this->y = y;
    }
};

void display(const Test &test) {
    cout << test.name << endl;
    cout << test.x << endl;
    cout << test.y << endl;
    cout << test.p << endl;

}

void setp(Test &test)
{
    test.p = (test.y)?test.x*100.0/test.y:-1;
}

void accumulate(Test &t1,const Test &t2)
{
    t1.x+=t2.x;
    t1.y+=t2.y;
}

int main ()
{
    Test t1,t2("unknown",1,2);
    cout<<"t1:"<<endl;
    display(t1);
    cout<<"t2:"<<endl;
    display(t2);
    setp(t1);
    cout<<"t1 after setp:"<<endl;
    display(t1);
    accumulate(t1,t2);
    setp(t1);
    cout<<"t1 after accumulate and setp:"<<endl;
    display(t1);

    return 0;
}
