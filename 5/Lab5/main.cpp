#include <iostream>
#include <string>
#include <cstring> // ��� strcpy()
#include <cctype> // ��� toupper()

using namespace std;

string & to_upper(string & str)
{
    for (int i = 0; i < str.length(); i++)
    {
        str[i] = toupper(str[i]);
    }
    return str;
}

int main()
{
    string str;
    cout << "Enter a string (q to quit): ";
    while (true)
    {
        getline(cin, str);
        if (str != "q")
        {
            to_upper(str);
            cout << str << "\nNext string (q to quit): ";
        }
        else
        {
            cout << "\nBye.\n";
            break;
        }
    }
    return 0;
}
