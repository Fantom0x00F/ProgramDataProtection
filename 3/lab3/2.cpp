#include <iostream>
#include <ctype.h>

using namespace std;

int main()
{
    char sym;
    string resultText = "";
    while(cin.get(sym))
    {
        if (!isdigit(sym))
        {
            if (isalnum(sym))
            {
                if (isupper(sym))
                    sym = tolower(sym);
                else
                    sym = toupper(sym);
            }
            resultText+=sym;
        }
    }
    cout<<resultText;
}
