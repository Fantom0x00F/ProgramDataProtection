#include<iostream>
#include <iomanip>

using namespace std;

int main()
{
    char ch;
    cout << "Enter one of the following choices: " <<endl
         << "a) apple" << setw(20) << "b) banana" << endl
         << "c) citrus" << setw(20) << "d) date" << endl;
    cout << "Please enter a, b, c or d: ";
    while (cin >> ch)
    {
        switch (ch)
        {
        case 'a':
            cout<<"I like apple!"<<endl;
            break;
        case 'b':
            cout<<"I like banana!"<<endl;
            break;
        case 'c':
            cout<<"I like citrus!"<<endl;
            break;
        case 'd':
            cout<<"I like date!"<<endl;
            break;
        }
        cout << "Please enter a, b, c or d: ";
    }
}
