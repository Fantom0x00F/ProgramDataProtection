#include<iostream>
#include<fstream>
#include<cstdlib>

using namespace std;

int main()
{
    ifstream in("data.txt");
    if (!in.is_open())
    {
        cout<<"Cannot open file!";
        exit(EXIT_FAILURE);
    }
    double summ = 0;
    int countOfNumbers = 0;
    double num;
    while(!in.eof())
    {
        in>>num;
        if (in.fail())
        {
            cout<<"Incompatible types"<<endl;
            break;
        }
        if (in.good())
        {
            summ+=num;
            countOfNumbers++;
        }
        else
            cout<<"Something happens"<<endl;
    }
    if (!in.eof())
        cout<<"There is more values in file"<<endl;
    cout<<"Count of numbers: "<<countOfNumbers<<endl;
    cout<<"Sum of numbers: "<<summ<<endl;
    in.close();
    return 0;
}
