#include<iostream>

using namespace std;

#define SIZE 35
#define countBops 3

struct bop
{
    char fullname[SIZE];
    char title[SIZE];
    char boqname[SIZE];
    int preference;
};

int main()
{
    char ch;
    bop BOP[countBops] = {"First programmer", "programmer", "FM", 0,
                    "Second programmer", "programmer", "SM", 1,
                    "Unique tester","tester","UT",2};
    cout << "Benevolent order of Programmers Report" << endl
         << "a. display by name b. display by title" << endl
         << "c. display by boqname d. display by preference" << endl;

    cout << "Enter your choise: ";
    do
    {
        cin.get(ch);
        cin.get();
        switch(ch)
        {
        case 'a':
            for (int i = 0; i < countBops; i++)
                cout << BOP[i].fullname << endl;
            break;
        case 'b':
            for (int i = 0; i < countBops; i++)
                cout << BOP[i].title << endl;
            break;
        case 'c':
            for (int i = 0; i < countBops; i++)
                cout << BOP[i].boqname << endl;
            break;
        case 'd':
            for (int i = 0; i < countBops; i++)
                switch (BOP[i].preference)
                {
                case 0:
                    cout << BOP[i].fullname << endl;break;
                case 1:
                    cout << BOP[i].title << endl;break;
                case 2:
                    cout << BOP[i].boqname << endl;break;
                }
            break;
        }
        if (ch != 'q')
            cout << "Next choice:"<<endl;
    }
    while (ch != 'q');
    cout<<"Bye!"<<endl;
    return 0;
}
