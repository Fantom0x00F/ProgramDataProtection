#include <iostream>
#include <ctype.h>

using namespace std;

short getType(char sym)
{
    if (isalpha(sym))
        return 0;
    if (isspace(sym))
        return 1;
    if (isdigit(sym))
        return 2;
    if (ispunct(sym))
        return 3;
    return 4;
}

int main()
{
    //0-�����
    //1-�������
    //2-�����
    //3-����������
    //4-���������
    //5-�����
    int countOfTypes[] = {0,0,0,0,0,0};
    char sym;
    while (cin.get(sym))
    {
        countOfTypes[5]++;
        countOfTypes[getType(sym)]++;
    }
    cout<<"Count of all symbols:"<<countOfTypes[5]<<endl;
    cout<<"Count of letters:"<<countOfTypes[0]<<endl;
    cout<<"Count of spaces:"<<countOfTypes[1]<<endl;
    cout<<"Count of digits:"<<countOfTypes[2]<<endl;
    cout<<"Count of punctuations:"<<countOfTypes[3]<<endl;
    cout<<"Count of other symbols:"<<countOfTypes[4]<<endl;
    return 0;
}
