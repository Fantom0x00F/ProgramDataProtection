#include <iostream>

#define Max 5

using namespace std;

int main()
{
    double fish[Max];
    double summ = 0;
    cout<<"Input weights (maximum "<<Max<<",\"q\" for exit):\n";
    int i = 0;
    cout<<"Fish #1: ";
    while( i<Max && cin>>fish[i])
    {
        summ+=fish[i];
        if (++i<Max)
            cout<<"Fish #"<<i+1<<": ";
    }
    if (i==0)
        cout<<"No fish"<<endl;
    cout<<"Average weight of fish is "<<summ/i<<endl;
    return 0;
}
