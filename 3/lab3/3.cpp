#include <iostream>
#include <ctype.h>

using namespace std;

bool isvowel(char c)
{
    return (c=='a'||c=='A'||c=='e'||c=='E'||c=='i'||c=='I'||c=='o'||c=='O'||c=='u'||c=='U'||c=='y'||c=='Y')?true:false;
}

int main()
{
    string word;
    int startVowel = 0,startConstrant = 0,startOther = 0;
    while(cin>>word)
    {
        if (isalpha(word[0]))
        {
            if (isvowel(word[0]))
                startVowel++;
            else
                startConstrant++;
        }
        else
            startOther++;
    }
    cout<<startVowel<<" words beginnig with vowels"<<endl;
    cout<<startConstrant<<" words beginning with consonants"<<endl;
    cout<<startOther<<" others"<<endl;
    return 0;
}
