#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cstring>

using namespace std;

int main()
{
    ifstream in;
    char fileName[200]="\0";
    int type = 0;
    while(strcmp(fileName,"q") !=0)
    {
        cout<<"Enter file name:";
        cin>>fileName;
        if (strcmp(fileName,"q") ==0)
            break;
        in.open(fileName);
        if (!in.is_open())
        {
            cout<<"Cannot open file!"<<endl;
            continue;
        }
        cout<<"Enter the type: \n1 - count of symbols\n2 - count of word\n3 - count of paragraph\n>";
        cin>>type;
        if (type == 1)
        {
            int countOfSyms = 0;
            char sym;
            while (in.get(sym))
                countOfSyms++;
            cout<<"Count of symbols: "<<countOfSyms<<endl;
        }
        else if (type == 2)
        {
            int countOfWords = 0;
            string word;
            while (in>>word)
                countOfWords++;
            cout<<"Count of words: "<<countOfWords<<endl;
        }
        else if (type == 3)
        {
            int countOfParag = 0;
            string paragraph;
            while (getline(in,paragraph))
                countOfParag++;
            cout<<"Count of paragraph: "<<countOfParag<<endl;
        }
        in.close();
    }
    return 0;
}
