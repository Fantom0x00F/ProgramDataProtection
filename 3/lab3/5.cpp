#include <iostream>

#define Max 5

using namespace std;

int main()
{
    int golf[Max];
    cout<<"Type the results of games of golf.\nYou need to type "<<Max<<" values\n";
    for (int i = 0 ; i < Max ; i++)
    {
        cout<<"Round "<<i+1<<": ";
        while(!(cin>>golf[i]))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;
            cout<<"Incorrect value!"<<endl;
            cout<<"Round "<<i+1<<": ";
        }
    }
    double average = 0;
    for (int i = 0 ; i < Max ; i++)
        average+=golf[i];
    cout<<"Average value "<<average/Max;
    return 0;
}
