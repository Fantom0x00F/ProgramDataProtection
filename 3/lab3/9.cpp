#include<iostream>
#include<fstream>

#define SIZE 35

using namespace std;

struct infoCar
{
    char model[SIZE];
    int year;
    int price;
};

int main()
{
    int countCar = 0;
    cout<<"Type the number of cars:";
    cin>>countCar;
    if (countCar<=0)
    {
        cout<<"Incorrect value";
        return -1;
    }
    ofstream out("cars.txt");
    infoCar cars[countCar];
    for (int i = 0 ; i < countCar ; i++)
    {
        cout<<"Car #"<<i+1<<endl;
        cout<<"Enter the model of automobile: ";
        cin>>cars[i].model;
        cout<<"Enter the model year: ";
        cin>>cars[i].year;
        cout<<"Enter the price of automobile: ";
        cin>>cars[i].price;
    }
    cout<<"Out:"<<endl;
    for (int i = 0 ; i < countCar ; i++)
    {
        cout<<"Model: "<<cars[i].model<<endl;
        out<<"Model: "<<cars[i].model<<endl;
        cout<<"Year: "<<cars[i].year<<endl;
        out<<"Year: "<<cars[i].year<<endl;
        cout<<"Price: "<<cars[i].price<<endl;
        out<<"Price: "<<cars[i].price<<endl;
        cout<<endl;
        out<<endl;
    }
    out.close();
    return 0;
}
