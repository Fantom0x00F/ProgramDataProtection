#include <iostream>

using namespace std;

struct Expression{
    virtual double evaluate() const= 0;
};

struct Number : Expression{
    Number(double value) : value(value){}

    double evaluate() const
    {
        return value;
    }
private:
    double value;
};

struct BinaryOperation : Expression{
    BinaryOperation(Expression * left, char op, Expression * right)
        : left(left), op(op), right(right)
    { }

    double evaluate() const
    {
        switch(op)
        {
        case('+'):
            return left->evaluate()+right->evaluate();
        case('-'):
            return left->evaluate()-right->evaluate();
        case('*'):
            return left->evaluate()*right->evaluate();
        case('/'):
            return left->evaluate()/right->evaluate();
        }
    }

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

private:
    Expression * left;
    Expression * right;
    char op;
};

bool check_equals(Expression const *left, Expression const *right)
{
    if (sizeof(&left) == sizeof(&right))
        return true;
    return false;
}

int main()
{
    Expression *fe,*se,*te,*eval;

    Number first(2),second(10),third(5);
    fe= &first;
    se= &second;
    te= &third;

    BinaryOperation bop(fe,'+',se);
    eval = &bop;
    BinaryOperation mul(eval,'*',te);


    cout<<mul.evaluate()<<endl;

    cout<<check_equals(fe,se)<<endl;
    return 0;
}
