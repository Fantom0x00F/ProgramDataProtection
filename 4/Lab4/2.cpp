#include<iostream>
#define N 10

using namespace std;

int get (int golf[])
{
 int temp, i;
 for (i = 0; i < N; i++)
 {
  cout  << "Enter " << i+1 << " result (\"q\" for exit)";
  if (!(cin >> temp))
  {
   cin.clear();
   while (cin.get() != '\n')
    continue;
   break;
  }
  golf[i] = temp;
 }
 return i;
}

double mean (int golf[],int countGame)
{
 int sum = golf[0];
 for (int i = 1; i < countGame; i++)
  sum += golf[i];
 return 1.0*sum/countGame;
}

void printArray(int arr[],int sizeOfArray)
{
    for (int i = 0 ; i < sizeOfArray ; i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

int main()
{
 double meanValue;
 int golf[N];
 int countGame;
 countGame = get(golf);
 meanValue = mean(golf, countGame);
 printArray(golf,countGame);
 cout<<"Mean value: "<<meanValue;
 return 0;
}
