#include <iostream>

using namespace std;

double Harmonic_mean(double x,double y)
{
    return (2.0*x*y)/(x+y);
}

int main()
{
    double x;
    double y;
    cin>>x>>y;
    while(x!=0 || y!=0)
    {
        cout<<"Harmonic mean: "<<Harmonic_mean(x,y)<<endl;
        cin>>x>>y;
    }
    cout << "Hello world!" << endl;
    return 0;
}
