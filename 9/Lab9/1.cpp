#include <iostream>

using namespace std;

class Integer{
public:
    Integer(int val):value(val){}

    const Integer& operator++();

    const Integer& operator++(int);

    friend ostream& operator<<(ostream& os, const Integer& i);

private:
    int value;
};

const Integer& Integer::operator++() {
    value++;
    return *this;
}

const Integer& Integer::operator++(int) {
    Integer old(value);
    value++;
    return old;
}


ostream& operator<<(ostream& os, const Integer& i){
    return os<<i.value;
}

int main()
{
    Integer i(5);
    cout <<i++<<" "<<i<<endl;
    return 0;
}
