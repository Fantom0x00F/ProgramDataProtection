#include <iostream>

using namespace std;

class Integer{
public:
    Integer(int val):value(val){}

    const Integer& operator--();

    const Integer& operator--(int);

    const Integer& operator++();

    const Integer& operator++(int);

    const Integer& operator+(const Integer& right) const;

    const Integer& operator-(const Integer& right) const;

    friend ostream& operator<<(ostream& os, const Integer& i);

    void print(ostream& os){
        os<<value;
    }

    ~Integer()
    {
        cout<<"Dest"<<endl;
    }

private:
    int value;
};

const Integer& Integer::operator++(){
    value++;
    return *this;
}

const Integer& Integer::operator++(int) {
    Integer old(value);
    value++;
    return old;
}

const Integer& Integer::operator--() {
    value--;
    return *this;
}

const Integer& Integer::operator--(int) {
    Integer *old = new Integer(value);
    value--;
    return *old;
}

const Integer& Integer::operator+(const Integer& right) const{
    Integer *res = new Integer(value+right.value);
    cout<<"1:"<<this->value<<endl<<"2:"<<right.value<<endl<<res->value;
    return *res;
}

const Integer& Integer::operator-(const Integer& right) const{
    Integer res(value-right.value);
    return res;
}

ostream& operator<<(ostream& os, const Integer& i){
    return os<<i.value;
}

int main()
{
    Integer i(5);
    cout<<"------------------"<<endl;
    Integer k = ((i++) + (--i));
    cout<<k<<endl;
    cout<<"------------------"<<endl;
    return 0;
}
