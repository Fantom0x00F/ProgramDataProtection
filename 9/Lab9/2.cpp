#include <iostream>

using namespace std;

class Integer{
public:
    Integer(int val):value(val){}

    const Integer& operator+(const Integer& right);

    friend ostream& operator<<(ostream& os, const Integer& i);

    void print(ostream& os){
        os<<value;
    }

private:
    int value;
};

const Integer& Integer::operator+(const Integer& right){
    Integer res(value+right.value);
    return res;
}

ostream& operator<<(ostream& os, const Integer& i){
    return os<<i.value;
}

int main()
{
    Integer i(5),j(6);
    Integer k = i+j;
    k.print(cout);
    return 0;
}
