#include <iostream>

using namespace std;

class Integer{
public:
    Integer(int val):value(val){}

    const Integer& operator--();

    const Integer& operator--(int);

    const Integer& operator--();

    const Integer& operator--(int);

    const Integer& operator+(const Integer& right) const;

    const Integer& operator-(const Integer& right) const;

    friend ostream& operator<<(ostream& os, const Integer& i);

    void print(ostream& os){
        os<<value;
    }

private:
    int value;
};

const Integer& Integer::operator+(const Integer& right) const{
    Integer res(value+right.value);
    return res;
}

const Integer& Integer::operator-(const Integer& right) const{
    Integer res(value-right.value);
    return res;
}

ostream& operator<<(ostream& os, const Integer& i){
    return os<<i.value;
}

int main()
{
    Integer i(5),j(6),l(11);
    Integer k = (i+j-l);
    k.print(cout);
    return 0;
}
