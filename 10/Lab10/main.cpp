#include <iostream>

using namespace std;

struct Number;
struct BinaryOperation;

struct Visitor {
    virtual void visitNumber(Number const * number) = 0;
    virtual void visitBinaryOperation(BinaryOperation const * operation) = 0;
    virtual ~Visitor() { }
};


struct Expression{
    virtual double evaluate() const= 0;
    virtual void visit(Visitor * vistitor) const = 0;
    virtual int getPrior() const = 0;
};

struct Number : Expression{
public:
    Number(double value) : value(value){}

    double get_value() const { return value; }

    double evaluate() const
    {
        return value;
    }

    void visit(Visitor * visitor) const { visitor->visitNumber(this); }

    int getPrior() const
    {
        return 3;
    }

private:
    double value;
};

struct BinaryOperation : Expression{
public:
    BinaryOperation(Expression * left, char op, Expression * right)
        : left(left), op(op), right(right)
    { }

    double evaluate() const
    {
        switch(op)
        {
        case('+'):
            return left->evaluate()+right->evaluate();
        case('-'):
            return left->evaluate()-right->evaluate();
        case('*'):
            return left->evaluate()*right->evaluate();
        case('/'):
            return left->evaluate()/right->evaluate();
        }
    }

    Expression const * get_left() const { return left; }
    Expression const * get_right() const { return right; }
    char get_op() const { return op; }

    void visit(Visitor * visitor) const { visitor->visitBinaryOperation(this); }

    int getPrior() const
    {
        if (op == '+' || op == '-')
            return 1;
        return 2;
    }

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

private:
    Expression * left;
    Expression * right;
    char op;
};

struct PrintVisitor : Visitor {
    void visitNumber(Number const * number)
    {
        cout<<number->get_value();
    }

    void visitBinaryOperation(BinaryOperation const * bop)
    {
        int leftPrior = bop->get_left()->getPrior();
        if (leftPrior < bop->getPrior())
        {
            cout<<"(";
            bop->get_left()->visit(this);
            cout<<")";
        }
        else
            bop->get_left()->visit(this);

        cout<<bop->get_op();

        int rightPrior = bop->get_right()->getPrior();

        if (rightPrior < bop->getPrior())
        {
            cout<<"(";
            bop->get_right()->visit(this);
            cout<<")";
        }
        else
            bop->get_right()->visit(this);
    }
};

int main()
{
	Visitor *visitor = new PrintVisitor();
    Number *n1 = new Number(1.0);
    n1->visit(visitor);
    cout<<endl;
    Number *n2 = new Number(2.0);
    Number *n3 = new Number(3.0);
    Number *n4 = new Number(4.0);
    Expression *e1 = new BinaryOperation(n1, '-', n2);
    e1->visit(visitor);
    cout<<endl;
    Expression *e2 = new BinaryOperation(e1, '*', n3);
    e2->visit(visitor);
    cout<<endl;
    Expression *e3 = new BinaryOperation(n4, '+', e2);
    e3->visit(visitor);
    cout<<endl;
    return 0;
}
