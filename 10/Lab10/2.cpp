#include <iostream>

using namespace std;

struct Visitor {
    virtual void visitNumber(Number const * number) = 0;
    virtual void visitBinaryOperation(BinaryOperation const * operation) = 0;
    virtual ~Visitor() { }
};

struct PrintVisitor : Visitor {
    void visitNumber(Number const * number)
    {
        cout<<number->
    }

    void visitBinaryOperation(BinaryOperation const * bop)
    {
        /* ... */
    }
};


struct Expression{
    virtual double evaluate() const= 0;
    virtual void visit(Visitor * vistitor) const = 0;
    virtual void print() const = 0;
};

struct Number : Expression{
    Number(double value) : value(value){}

    double evaluate() const
    {
        return value;
    }

    void visit(Visitor * visitor) const { visitor->visitNumber(this); }

private:
    double value;
};

struct BinaryOperation : Expression{
    BinaryOperation(Expression * left, char op, Expression * right)
        : left(left), op(op), right(right)
    { }

    double evaluate() const
    {
        switch(op)
        {
        case('+'):
            return left->evaluate()+right->evaluate();
        case('-'):
            return left->evaluate()-right->evaluate();
        case('*'):
            return left->evaluate()*right->evaluate();
        case('/'):
            return left->evaluate()/right->evaluate();
        }
    }

    void visit(Visitor * visitor) const { visitor->visitBinaryOperation(this); }

    ~BinaryOperation()
    {
        delete left;
        delete right;
    }

private:
    Expression * left;
    Expression * right;
    char op;
};

int main()
{
    Expression *fe,*se,*te,*eval;

    Number first(2),second(10),third(5);
    fe= &first;
    se= &second;
    te= &third;

    BinaryOperation bop(fe,'+',se);
    eval = &bop;
    BinaryOperation mul(eval,'*',te);

    mul.print();

    return 0;
}
